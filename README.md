## Dependencies
All the dependencies are standard python packages:
- Pytorch, Numpy, Matplotlib

## Problem Settings
The general goal is to learn a function that can represent a given shape. For example, points on a 2-d sphere can be represented by $f([x_1, x_2]) = x_1^2 + x_2^2 - r^2 = 0$. 

Here is one possible problem setting.

### Dataset 
```
_dataloader.py
```

Suppose we have the points near a given shape $x_{data}$ (red points in the following figure), and their nearest points on the shape $x_{label}$ (blue points).

<img src="/docs/data.PNG" width="50%">

Given this data, we want to learn a function $f([x_1, x_2])$ that all points on the shape satisfy $f([x_1, x_2]) = 0$.

In _dataloader.py, we generates the data of $x_1^2 + x_2^2 - 1 = 0$.

### The projection to find the point on the shape 
```
_iterative.py
```
If we have a function $f(x)$ (here $x$ can be a vector) and a point near the surface of $f(x) = 0$, we can use the following projection algorithm to find its nearest point on $f(x) = 0$.

<img src="/docs/proj_algo.PNG" width="50%">

This algorithm is implemented in _iterative.py.

### Networks  
```
_networks.py
```
We can use a neural network to represent the function $f(x)$. The input is a 2-d vector, and the output is a scalar. In _networks.py, Simple_NN is a simple network consisting of several linear layers and non-linear activations. The weights of the linear layers are variables that can be learned so that $f(x)=0$ fits the shape we desire.

### Training the network
```
_training.py
sphere_train.py
```
Given $f(x)$ (the network) and input data $x_{data}$, we can use Algo 1 to get the point $x_{nn} = IterativeNeuralProjection(f(\cdot), x_{data})$ that is on the shape defined by $f(x) = 0$. The goal is to have $x_{nn}$ close to the point on the groundtruth shape ($x_{label}$). We can use the difference between $x_{nn}$ and $x_{label}$ to form the loss function to train the learnable weights in the network.  

### Results
```
sphere_visualization.py
```
Run sphere_train.py, we will get the learned weights. Then, we can run sphere_visualization.py to see the learned results:
(Yellow: $x_{data}$; Blue: $x_{nn}$; Green: $x_{label}$.)

<img src="/docs/v1.PNG" width="50%">

We can also plot $f([x_1, x_2])$. The curve of $f([x_1, x_2]) = 0$ is very close to that of $x_1^2 + x_2^2 = 1$.

<img src="/docs/v2.PNG" width="50%">